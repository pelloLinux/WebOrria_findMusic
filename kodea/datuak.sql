-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-12-2015 a las 11:44:01
-- Versión del servidor: 10.0.17-MariaDB
-- Versión de PHP: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `datuak`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informazioa`
--

CREATE TABLE `informazioa` (
  `taldea` varchar(50) NOT NULL,
  `diskoa` varchar(50) NOT NULL,
  `abestia` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `informazioa`
--

INSERT INTO `informazioa` (`taldea`, `diskoa`, `abestia`) VALUES
('U2', 'Sons of Innocence', 'The Miracle'),
('U2', 'Sons of Innocence', 'Every Breaking Wave'),
('U2', 'Sons of Innocence', 'California (There Is No End To Love)'),
('U2', 'Sons of Innocence', 'Song for Someone'),
('U2', 'Sons of Innocence', 'Iris (Hold Me Close)'),
('U2', 'Sons of Innocence', 'Volcano'),
('U2', 'Sons of Innocence', 'Raised By Wolves'),
('U2', 'Sons of Innocence', 'Cedarwood Road'),
('U2', 'Sons of Innocence', 'Sleep Like a Baby Tonight'),
('U2', 'Sons of Innocence', 'This is Where You Can Reach Me Now'),
('U2', 'Sons of Innocence', 'The Troubles'),
('Bon Jovi', 'Have A Nice Day', 'Have A Nice Day'),
('Bon Jovi', 'Have A Nice Day', 'I Want to Be Loved'),
('Bon Jovi', 'Have A Nice Day', 'Welcome to Wherever You Are'),
('Bon Jovi', 'Have A Nice Day', 'Who Says You Cannot Go Home?'),
('Bon Jovi', 'Have A Nice Day', 'Last Man Standing'),
('Bon Jovi', 'Have A Nice Day', 'Bells of Freedom'),
('Bon Jovi', 'Have A Nice Day', 'Wildflower'),
('Bon Jovi', 'Have A Nice Day', 'Last Cigarette'),
('Bon Jovi', 'Have A Nice Day', 'I Am'),
('Bon Jovi', 'Have A Nice Day', 'Complicated'),
('Bon Jovi', 'Have A Nice Day', 'Novocaine'),
('Bon Jovi', 'Have A Nice Day', 'Story of My Life'),
('Bon Jovi', 'Have A Nice Day', 'Nothing'),
('AC/DC', 'Back in Black', 'Hell Bells'),
('AC/DC', 'Back in Black', 'Shoot to Thrill'),
('AC/DC', 'Back in Black', 'What Do You Do for Money Honey'),
('AC/DC', 'Back in Black', 'Giving The Dog a Bone'),
('AC/DC', 'Back in Black', 'Let Me Put my Love Into You'),
('AC/DC', 'Back in Black', 'Back in Black'),
('AC/DC', 'Back in Black', 'You Shook Me All Night Long'),
('AC/DC', 'Back in Black', 'Have A Drink on Me'),
('AC/DC', 'Back in Black', 'Shake a Leg'),
('AC/DC', 'Back in Black', 'Rock n Roll Is Not Noise Pollution'),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'Intro'),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'El de en medio de los RUM DMC (se ha aparecido en '),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'Pasion de talibanes'),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'Gente V.I.P'),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'Busco algun lugar (con Jerry Coke)'),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'Maniquies & plastico'),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'Tesis de abril'),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'La mazorka mecanica'),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'Confesiones'),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'Los hijos de Ivan Drago'),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'Retales en mi cuaderno'),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'Fear of a Mazorka Planet'),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'C.O.P.$'),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'Actor secundario'),
('Los Chikos del Maiz', 'Pasion de Talibanes', 'Abierto hasta el amanecer'),
('Rita Ora', 'ORA', 'Facemelt'),
('Rita Ora', 'ORA', 'Roc the Life'),
('Rita Ora', 'ORA', 'How We Do (Party)'),
('Rita Ora', 'ORA', 'R.I.P'),
('Rita Ora', 'ORA', 'Radioactive'),
('Rita Ora', 'ORA', 'Shine Ya Light'),
('Rita Ora', 'ORA', 'Love and War'),
('Rita Ora', 'ORA', 'Uneasy'),
('Rita Ora', 'ORA', 'Fall in Love'),
('Rita Ora', 'ORA', 'Been Lying'),
('Rita Ora', 'ORA', 'Hello, Hi, Goodbye'),
('Rita Ora', 'ORA', 'Hot Right Now'),
('David Guetta', 'Fuck Me I am Famous', 'Just for One Day (Heroes)'),
('David Guetta', 'Fuck Me I am Famous', 'Shout'),
('David Guetta', 'Fuck Me I am Famous', 'Shake It'),
('David Guetta', 'Fuck Me I am Famous', 'Fucking Track'),
('David Guetta', 'Fuck Me I am Famous', 'Satisfaction'),
('David Guetta', 'Fuck Me I am Famous', 'Distortion'),
('David Guetta', 'Fuck Me I am Famous', 'Heart Beat'),
('David Guetta', 'Fuck Me I am Famous', 'Sunshine'),
('David Guetta', 'Fuck Me I am Famous', 'Sometimes'),
('David Guetta', 'Fuck Me I am Famous', 'If You Give Me Love'),
('David Guetta', 'Fuck Me I am Famous', 'Ghetto Blaster'),
('David Guetta', 'Fuck Me I am Famous', 'Stock Exchange'),
('David Guetta', 'Fuck Me I am Famous', 'Who Needs Sleep Tonight'),
('David Guetta', 'Fuck Me I am Famous', 'Bye Bye Superman'),
('David Guetta', 'Fuck Me I am Famous', 'Bucci Bag'),
('Bruno Mars', 'Doo-Wops and Hooligans', 'Grenade'),
('Bruno Mars', 'Doo-Wops and Hooligans', 'Just the Way You Are'),
('Bruno Mars', 'Doo-Wops and Hooligans', 'Our First Time'),
('Bruno Mars', 'Doo-Wops and Hooligans', 'Runaway Baby'),
('Bruno Mars', 'Doo-Wops and Hooligans', 'The Lazy Song'),
('Bruno Mars', 'Doo-Wops and Hooligans', 'Marry You'),
('Bruno Mars', 'Doo-Wops and Hooligans', 'Talking to the Moon'),
('Bruno Mars', 'Doo-Wops and Hooligans', 'Liquor Store Blues'),
('Bruno Mars', 'Doo-Wops and Hooligans', 'Count on Me'),
('Bruno Mars', 'Doo-Wops and Hooligans', 'The Other Side');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
