<?php
	$datua = $_GET['izena'];//$datua aldagaian taldearen izena edo diskaren izena egon daiteke.
	$luzeraDatua = strlen($datua);//string-aren luzera	
	$datuaGeneroak = $_GET['g'];//billaketa mugatzeko generoen arabera
	
	$xml = simplexml_load_file("datuak.xml");
	foreach($xml->children() as $elementua){

		$taldea = $elementua -> izena;
		$diskoa = $elementua -> diskoa;
		$irudia = $elementua -> irudia;
		$generoa = $elementua -> generoa;


		if($datua == "" && $datuaGeneroak == ""){//generoa aukeratu gabe eta talderaen izena edo diskaren izena sartu gabe.
			//Diska guztiak erakutsi.
			echo "
			<div id = '".$taldea."' style ='float:left;margin:5px; text-align:center;' onclick = 'fitxaIkusi(this)'>
				<img src = '".$irudia."' height = '100' width = '100' alt =''/><br>
				<div class = 'text-primary text-uppercase'>
					<b>".$taldea."</b>
				</div>
				<div class = 'text-primary'>
					".$diskoa."
				</div>
				<div class = 'text-primary'>
					".$generoa."
				</div>
			</div>";
		}

		else if($datua != "" && $datuaGeneroak == ""){//taldearen izena edo diskoaren bitartez billaketa soilok

			if(stristr($datua, substr($taldea,0,$luzeraDatua)) || stristr($datua, substr($diskoa,0,$luzeraDatua))){

				echo "
				<div id = '".$taldea."' style ='float:left;margin:5px; text-align:center;' onclick = 'fitxaIkusi(this)'>
					<img src = '".$irudia."' height = '100' width = '100' alt =''/><br>
					<div class = 'text-primary text-uppercase'>
						<b>".$taldea."</b>
					</div>
					<div class = 'text-primary'>
						".$diskoa."
					</div>
					<div class = 'text-primary'>
						".$generoa."
					</div>
				</div>";
			}
		}

		else if($datua == "" && $datuaGeneroak != ""){//generoa aukeratuta soilik

			$bilatzekoGeneroak = explode("/", $datuaGeneroak);//bilatzeko erabiliko diren generoak
			$bilatzekoKop = count($bilatzekoGeneroak);//bilatzeko erabiliko den genero kopurua

			$taldearenGeneroak = explode("/", $generoa);//ebaluatzen hari den taldeak dituen generoak
			$taldearenGeneroKop = count($taldearenGeneroak);//ebaluatzen hari den taldeak dituen genero kopurua

			if($bilatzekoKop <= $taldearenGeneroKop){//bilaketa egiteko genero kopuruak, ebaluatzen hari den taldeak baino genero gutxiago edo berdin izan behar ditu.
				//bilaketa egiten hari garen genero guztiak, ebaluatzen hari garen taldeak badituela egiaztatu
				$generoBerdinak=1;
				for ($i=0; $i < $bilatzekoKop; $i++) { 
					$momentukoGeneroaBilatuta = 0;
					for ($j=0; $j < $taldearenGeneroKop; $j++) { 

						if(strcmp($bilatzekoGeneroak[$i],$taldearenGeneroak[$j])==0){
							$momentukoGeneroaBilatuta = 1;
							break;
						}
					}
					if($momentukoGeneroaBilatuta == 0){
						$generoBerdinak=0;
						break;
					}
				}
				//bilaketa egiten hari garen genero guztiak, ebaluatzen hari garen taldeak baditu.
				if($generoBerdinak==1){

					echo "
					<div id = '".$taldea."' style ='float:left;margin:5px; text-align:center;' onclick = 'fitxaIkusi(this)'>
						<img src = '".$irudia."' height = '100' width = '100' alt =''/><br>
						<div class = 'text-primary text-uppercase'>
							<b>".$taldea."</b>
						</div>
						<div class = 'text-primary'>
							".$diskoa."
						</div>
						<div class = 'text-primary'>
							".$generoa."
						</div>
					</div>";
				}	
			}
		}
		else if($datua != "" && $datuaGeneroak != ""){//taldearen izena edo diskoaren bitartez eta generoaren bitartez billaketa

			if(stristr($datua, substr($taldea,0,$luzeraDatua)) || stristr($datua, substr($diskoa,0,$luzeraDatua))){

				$bilatzekoGeneroak = explode("/", $datuaGeneroak);//bilatzeko erabiliko diren generoak
				$bilatzekoKop = count($bilatzekoGeneroak);//bilatzeko erabiliko den genero kopurua

				$taldearenGeneroak = explode("/", $generoa);//ebaluatzen hari den taldeak dituen generoak
				$taldearenGeneroKop = count($taldearenGeneroak);//ebaluatzen hari den taldeak dituen genero kopurua

				if($bilatzekoKop <= $taldearenGeneroKop){//bilaketa egiteko genero kopuruak, ebaluatzen hari den taldeak baino genero gutxiago edo berdin izan behar ditu.
					//bilaketa egiten hari garen genero guztiak, ebaluatzen hari garen taldeak badituela egiaztatu
					$generoBerdinak=1;
					for ($i=0; $i < $bilatzekoKop; $i++) { 
						$momentukoGeneroaBilatuta = 0;
						for ($j=0; $j < $taldearenGeneroKop; $j++) { 

							if(strcmp($bilatzekoGeneroak[$i],$taldearenGeneroak[$j])==0){
								$momentukoGeneroaBilatuta = 1;
								break;
							}
						}
						if($momentukoGeneroaBilatuta == 0){
							$generoBerdinak=0;
							break;
						}
					}
					//bilaketa egiten hari garen genero guztiak, ebaluatzen hari garen taldeak baditu.
					if($generoBerdinak==1){

						echo "
						<div id = '".$taldea."' style ='float:left;margin:5px; text-align:center;' onclick = 'fitxaIkusi(this)'>
							<img src = '".$irudia."' height = '100' width = '100' alt =''/><br>
							<div class = 'text-primary text-uppercase'>
								<b>".$taldea."</b>
							</div>
							<div class = 'text-primary'>
								".$diskoa."
							</div>
							<div class = 'text-primary'>
								".$generoa."
							</div>
						</div>";
					}	
				}
			}
		}	
	}
?>
