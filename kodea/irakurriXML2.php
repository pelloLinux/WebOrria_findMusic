<?php

	$xml = simplexml_load_file("datuak.xml"); //XML fitxategia ireki
	$taldeKop = count($xml->children()); //Kontatu zenbat "haur" diruen
	$kont = 1;

	foreach($xml->children() as $elementua){

		if($kont > $taldeKop-5){ //Azkeneko bostetako talderen bat den begiratu

			$taldea = $elementua -> izena;
			$diskoa = $elementua -> diskoa;
			$irudia = $elementua -> irudia;
			$generoa = $elementua -> generoa;

					//Pantailaratu datuak.
					echo "
					<div id = '".$taldea."' style ='float:left;margin:5px; text-align:center;' onclick = 'fitxaIkusi(this)'>
						<img src = '".$irudia."' height = '100' width = '100' alt =''/><br>
						<div class = 'text-primary text-uppercase'>
							<b>".$taldea."</b>
						</div>
						<div class = 'text-primary'>
							".$diskoa."
						</div>
						<div class = 'text-primary'>
							".$generoa."
						</div>
					</div>";
		}
		$kont++;
	}
?>